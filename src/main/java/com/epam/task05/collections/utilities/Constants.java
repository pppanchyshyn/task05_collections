package com.epam.task05.collections.utilities;

public class Constants {

  public static final String[][] COUNTRY_CAPITAL = {
      {"ALBANIA", "Tirana"}, {"ANDORRA", "Andorra la Vella"},
      {"AUSTRIA", "Vienna"}, {"BELGIUM", "Brussels"},
      {"HERZEGOVINA", "Sarajevo"}, {"CROATIA", "Zagreb"},
      {"CZECH REPUBLIC", "Prague"}, {"DENMARK", "Copenhagen"},
      {"ESTONIA", "Tallinn"}, {"FINLAND", "Helsinki"},
      {"FRANCE", "Paris"}, {"GERMANY", "Berlin"},
      {"GREECE", "Athens"}, {"HUNGARY", "Budapest"},
      {"ICELAND", "Reykjavik"}, {"IRELAND", "Dublin"},
      {"ITALY", "Rome"}, {"LATVIA", "Riga"},
      {"LIECHTENSTEIN", "Vaduz"}, {"LITHUANIA", "Vilnius"},
      {"LUXEMBOURG", "Luxembourg"}, {"MACEDONIA", "Skopje"},
      {"MALTA", "Valletta"}, {"MONACO", "Monaco"},
      {"MONTENEGRO", "Podgorica"}, {"THE NETHERLANDS", "Amsterdam"},
      {"NORWAY", "Oslo"}, {"POLAND", "Warsaw"},
      {"PORTUGAL", "Lisbon"}, {"ROMANIA", "Bucharest"},
      {"SAN MARINO", "San Marino"}, {"SERBIA", "Belgrade"},
      {"SLOVAKIA", "Bratislava"}, {"SLOVENIA", "Ljujiana"},
      {"SPAIN", "Madrid"}, {"SWEDEN", "Stockholm"},
      {"SWITZERLAND", "Berne"}, {"UNITED KINGDOM", "London"}
  };
  public static final int CONTAINER_SIZE = 5;
  public static final int ITEMS_NUMBER = 10000;
  public static final int EXIT_MENU_ITEM = 3;

  private Constants() {
  }
}
