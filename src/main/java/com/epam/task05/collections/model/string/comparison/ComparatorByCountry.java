package com.epam.task05.collections.model.string.comparison;

import java.util.Comparator;

public class ComparatorByCountry implements Comparator<StringHolder> {

  @Override
  public int compare(StringHolder stringHolder1, StringHolder stringHolder2) {
    return stringHolder1.getCountry().compareTo(stringHolder2.getCountry());
  }
}
