package com.epam.task05.collections.model.string.comparison;

import com.epam.task05.collections.utilities.Constants;
import java.util.List;

public class CustomGenerator {

  public void generate(StringHolder[] array) {
    for (int i = 0; i < array.length; i++) {
      int random = (int) (Math.random() * Constants.COUNTRY_CAPITAL.length);
      array[i] = new StringHolder(Constants.COUNTRY_CAPITAL[random][0],
          Constants.COUNTRY_CAPITAL[random][1]);
    }
  }

  public void generate(List<StringHolder> arrayList, int size) {
    for (int i = 0; i < size; i++) {
      int random = (int) (Math.random() * Constants.COUNTRY_CAPITAL.length);
      arrayList.add(
          new StringHolder(Constants.COUNTRY_CAPITAL[random][0],
              Constants.COUNTRY_CAPITAL[random][1]));
    }
  }
}
