package com.epam.task05.collections.model;

public class StringContainer {

  private String[] elements;
  private int size;
  private static final int DEFAULT_CAPACITY = 10;
  private static final int INCREASE_RATIO = 2;

  public StringContainer() {
    this(DEFAULT_CAPACITY);
  }

  private StringContainer(int capacity) {
    elements = new String[capacity];
  }

  public void add(String value) {
    if (size >= elements.length) {
      String[] temp = new String[elements.length * INCREASE_RATIO];
      System.arraycopy(elements, 0, temp, 0, elements.length);
      elements = temp;
    }
    elements[size++] = value;
  }
}
