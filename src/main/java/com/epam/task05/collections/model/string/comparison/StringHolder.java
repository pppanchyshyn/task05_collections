package com.epam.task05.collections.model.string.comparison;

public class StringHolder {

  private String country;
  private String capital;

  StringHolder(String country, String capital) {
    this.country = country;
    this.capital = capital;
  }

  public String getCountry() {
    return country;
  }

  public String getCapital() {
    return capital;
  }

  public String toString() {
    return String.format("%s - %s", getCountry(), getCapital());
  }
}

