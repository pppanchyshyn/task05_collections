package com.epam.task05.collections.model;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class CustomDeque<T> implements Iterable<T> {

  private class Node<S> {

    Node<S> previous;
    Node<S> next;
    private final S item;

    Node(S item) {
      this.item = item;
    }

    void connectNext(Node<S> newOne) {
      this.next = newOne;
      newOne.previous = this;
    }
  }

  private class CustomDequeIterator implements Iterator<T> {

    private Node<T> current = firstElement;

    public boolean hasNext() {
      return current != null;
    }

    public T next() {
      if (!hasNext()) {
        throw new NoSuchElementException();
      }
      T item = current.item;
      current = current.next;
      return item;
    }
  }

  private Node<T> firstElement;
  private Node<T> lastElement;
  private int size;

  public CustomDeque() {
  }

  public Iterator<T> iterator() {
    return new CustomDequeIterator();
  }

  private int getSize() {
    return size;
  }

  private boolean isEmpty() {
    return getSize() == 0;
  }

  public void addFirst(T item) {
    Node<T> previousFirstElement = firstElement;
    Node<T> newFirstElement = new Node<>(item);
    if (previousFirstElement != null) {
      newFirstElement.connectNext(previousFirstElement);
    } else {
      lastElement = newFirstElement;
    }
    firstElement = newFirstElement;
  }

  public void addLast(T item) {
    Node<T> newLastElement = new Node<>(item);
    Node<T> previousLastElement = lastElement;
    if (previousLastElement != null) {
      previousLastElement.connectNext(newLastElement);
    } else {
      firstElement = newLastElement;
    }
    lastElement = newLastElement;
    size++;
  }

  public void removeFirst() {
    if (isEmpty()) {
      throw new java.util.NoSuchElementException();
    }
    Node<T> previousFirstElement = firstElement;
    firstElement = previousFirstElement.next;
    previousFirstElement.next = null;
    if (firstElement != null) {
      firstElement.previous = null;
    }
    size--;
  }

  public void removeLast() {
    if (isEmpty()) {
      throw new java.util.NoSuchElementException();
    }
    Node<T> previousLastElement = this.lastElement;
    this.lastElement = previousLastElement.previous;
    previousLastElement.previous = null;
    if (this.lastElement != null) {
      this.lastElement.next = null;
    }
    size--;
  }
}
