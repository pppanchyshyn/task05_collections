package com.epam.task05.collections.controller;

import com.epam.task05.collections.model.StringContainer;
import com.epam.task05.collections.utilities.Constants;
import java.util.ArrayList;
import java.util.List;

public class StringContainerController implements Testable {

  private StringContainer stringContainer;
  private List<String> arrayList;

  public StringContainerController() {
    stringContainer = new StringContainer();
    arrayList = new ArrayList<>();
  }

  @Override
  public ArrayList<String> test() {
    ArrayList<String> testResults = new ArrayList<>();
    long startTime;
    long finishTime;
    testResults.add(String.format("Time for adding %d strings to: ", Constants.ITEMS_NUMBER));
    startTime = System.currentTimeMillis();
    for (int i = 0; i < Constants.ITEMS_NUMBER; i++) {
      stringContainer.add(String.format("String %d", i));
    }
    finishTime = System.currentTimeMillis();
    testResults.add(String.format(" - Custom String Container: %d ms", finishTime - startTime));
    startTime = System.currentTimeMillis();
    for (int i = 0; i < Constants.ITEMS_NUMBER; i++) {
      arrayList.add(String.format("String %d", i));
    }
    finishTime = System.currentTimeMillis();
    testResults.add(String.format(" - ArrayList<String>: %d ms", finishTime - startTime));
    testResults.add("");
    return testResults;
  }
}
