package com.epam.task05.collections.controller;

import com.epam.task05.collections.model.CustomDeque;
import java.util.ArrayList;

public class CustomDequeController implements Testable{
 private CustomDeque<String> customDeque;

  public CustomDequeController() {
    customDeque = new CustomDeque<>();
  }

  private String getString(CustomDeque<String> customDeque) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[ ");
    for (String item: customDeque
        ) {
     stringBuilder.append(item).append(" ");
    }
    stringBuilder.append("]");
    return stringBuilder.toString();
  }

  @Override
  public ArrayList<String> test() {
    ArrayList<String> testResults = new ArrayList<>();
    testResults.add("Adding str1 to Deque start");
    customDeque.addFirst("str1");
    testResults.add(getString(customDeque));
    testResults.add("Adding str2 to Deque start");
    customDeque.addFirst("str2");
    testResults.add(getString(customDeque));
    testResults.add("Adding str3 to Deque end");
    customDeque.addLast("str3");
    testResults.add(getString(customDeque));
    testResults.add("Adding str4 to Deque end");
    customDeque.addLast("str4");
    testResults.add(getString(customDeque));
    testResults.add("Remove first and last element of Deque");
    customDeque.removeFirst();
    customDeque.removeLast();
    testResults.add(getString(customDeque));
    testResults.add("");
    return testResults;
  }
}
