package com.epam.task05.collections.controller;

import com.epam.task05.collections.model.string.comparison.*;
import com.epam.task05.collections.utilities.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class StringComparisonController implements Testable {

  private CustomGenerator customGenerator;
  private StringHolder[] array;
  private List<StringHolder> arrayList;
  private ComparatorByCapital byCapital;
  private ComparatorByCountry byCountry;

  public StringComparisonController() {
    customGenerator = new CustomGenerator();
    array = new StringHolder[Constants.CONTAINER_SIZE];
    arrayList = new ArrayList<>();
    byCapital = new ComparatorByCapital();
    byCountry = new ComparatorByCountry();
  }

  private void setArray() {
    customGenerator.generate(array);
  }

  private void setArrayList() {
    customGenerator.generate(arrayList, Constants.CONTAINER_SIZE);
  }

  private void sort(StringHolder[] array, Comparator<StringHolder> comparator) {
    Arrays.sort(array, comparator);
  }

  private void sort(List<StringHolder> arrayList, Comparator<StringHolder> comparator) {
    arrayList.sort(comparator);
  }

  public ArrayList<String> test() {
    ArrayList<String> testResults = new ArrayList<>();
    setArray();
    testResults.add("Array after creating");
    testResults.add(Arrays.toString(array));
    sort(array, byCountry);
    testResults.add("Array after sorting by country");
    testResults.add(Arrays.toString(array));
    sort(array, byCapital);
    testResults.add("Array after sorting by capital");
    testResults.add(Arrays.toString(array));
    setArrayList();
    testResults.add("Array list after creating");
    testResults.add(Arrays.toString(arrayList.toArray()));
    sort(arrayList, byCountry);
    testResults.add("Array after sorting by country");
    testResults.add(Arrays.toString(arrayList.toArray()));
    sort(arrayList, byCapital);
    testResults.add("Array after sorting by capital");
    testResults.add(Arrays.toString(arrayList.toArray()));
    testResults.add("");
    return testResults;
  }
}