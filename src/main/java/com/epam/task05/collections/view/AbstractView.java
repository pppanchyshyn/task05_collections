package com.epam.task05.collections.view;

import com.epam.task05.collections.utilities.Constants;
import java.util.ArrayList;
import java.util.Scanner;
import org.apache.logging.log4j.*;

abstract class AbstractView {

  private Scanner input = new Scanner(System.in);
  Logger logger = LogManager.getLogger(Application.class);


  AbstractView() {
  }

  private int enterInteger() {
    int integer;
    while (true) {
      if (input.hasNextInt()) {
        integer = input.nextInt();
        break;
      } else {
        System.out.print("Not integer! Try again: ");
        input.nextLine();
      }
    }
    return integer;
  }

  int enterMenuItem() {
    int menuItem;
    while (true) {
      menuItem = enterInteger();
      if ((menuItem >= 0) && (menuItem <= Constants.EXIT_MENU_ITEM)) {
        break;
      } else {
        logger.info("Non-existent menu item. Try again: ");
        input.nextLine();
      }
    }
    return menuItem;
  }

 void print(ArrayList<String> arrayList) {
    for (String message : arrayList
        ) {
      logger.info(message);
    }
  }

  void printMenu() {
    logger.info("To test custom string container press 0");
    logger.info("To sort strings press 1");
    logger.info("To test custom Deque press 2");
    logger.info("To quite press 3");
    logger.info("\nMake your choice: ");
  }
}