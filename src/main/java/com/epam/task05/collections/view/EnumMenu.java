package com.epam.task05.collections.view;

import com.epam.task05.collections.controller.CustomDequeController;
import com.epam.task05.collections.controller.StringComparisonController;
import com.epam.task05.collections.controller.StringContainerController;
import com.epam.task05.collections.controller.Testable;
import com.epam.task05.collections.utilities.Constants;
import java.util.ArrayList;

public class EnumMenu extends AbstractView {

  enum Controllers {
    CONTAINER(new StringContainerController()),
    COMPARISON(new StringComparisonController()),
    DEQUE(new CustomDequeController());
    Testable testable;
    ArrayList<String> testResults;

    Controllers(Testable testable) {
      this.setTestable(testable);
      testResults = testable.test();
    }

    public void setTestable(Testable testable) {
      this.testable = testable;
    }
  }

  public void execute() {
    int menuItem;
    while (true) {
      printMenu();
      menuItem = enterMenuItem();
      if (menuItem == Constants.EXIT_MENU_ITEM) {
        logger.info("Good luck");
        break;
      }
      print(Controllers.values()[menuItem].testResults);
    }
  }
}
