package com.epam.task05.collections.view;

import com.epam.task05.collections.controller.CustomDequeController;
import com.epam.task05.collections.controller.StringComparisonController;
import com.epam.task05.collections.controller.StringContainerController;
import com.epam.task05.collections.controller.Testable;
import com.epam.task05.collections.utilities.Constants;
import java.util.HashMap;
import java.util.Map;

public class MapMenu extends AbstractView {

  private Map<Integer, Testable> map;

  public MapMenu() {
    map = new HashMap<>();
    map.put(0, new StringContainerController());
    map.put(1, new StringComparisonController());
    map.put(2, new CustomDequeController());
  }

  public void execute() {
    int menuItem;
    while (true) {
      printMenu();
      menuItem = enterMenuItem();
      if (menuItem == Constants.EXIT_MENU_ITEM) {
        logger.info("Good luck");
        break;
      }
      print(map.get(menuItem).test());
    }
  }
}
